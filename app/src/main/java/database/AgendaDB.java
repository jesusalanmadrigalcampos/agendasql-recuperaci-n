package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaDB {

    private Context context;
    private DBHelper helper;
    private SQLiteDatabase db;

    private String[] columnToRead = new String[]{
            DefinirTabla.Contacto._ID,
            DefinirTabla.Contacto.NOMBRE,
            DefinirTabla.Contacto.TELEFONO_1,
            DefinirTabla.Contacto.TELEFONO_2,
            DefinirTabla.Contacto.DOMICILIO,
            DefinirTabla.Contacto.NOTA,
            DefinirTabla.Contacto.FAVORITO
    };

    public AgendaDB(Context context){
        this.context = context;
        this.helper = new DBHelper(this.context);
    }

    public void openDatabase(){
        db = helper.getWritableDatabase();
    }

    public long insertarContacto(Contacto contacto){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.NOMBRE, contacto.getNombre());
        values.put(DefinirTabla.Contacto.TELEFONO_1, contacto.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO_2, contacto.getTelefono2());
        values.put(DefinirTabla.Contacto.DOMICILIO, contacto.getDomicilio());
        values.put(DefinirTabla.Contacto.NOTA, contacto.getNota());
        values.put(DefinirTabla.Contacto.FAVORITO, contacto.getFavorito());

        return db.insert(DefinirTabla.Contacto.TABLE_NAME,null,values);
    }

    public long actualizarContacto(Contacto contacto, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.NOMBRE, contacto.getNombre());
        values.put(DefinirTabla.Contacto.TELEFONO_1, contacto.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO_2, contacto.getTelefono2());
        values.put(DefinirTabla.Contacto.DOMICILIO, contacto.getDomicilio());
        values.put(DefinirTabla.Contacto.NOTA, contacto.getNota());
        values.put(DefinirTabla.Contacto.FAVORITO, contacto.getFavorito());
        String criterio = DefinirTabla.Contacto._ID + "=" + id;
        return db.update(DefinirTabla.Contacto.TABLE_NAME,values,criterio,null);
    }

    public long eliminarContacto(long id){
        String criterio = DefinirTabla.Contacto._ID + "=" + id;
        return db.delete(DefinirTabla.Contacto.TABLE_NAME,criterio,null);
    }

    public Contacto leerContacto(Cursor cursor){
        Contacto contacto = new Contacto();
        contacto.setID(cursor.getInt(0));
        contacto.setNombre(cursor.getString(1));
        contacto.setTelefono1(cursor.getString(2));
        contacto.setTelefono2(cursor.getString(3));
        contacto.setDomicilio(cursor.getString(4));
        contacto.setNota(cursor.getString(5));
        contacto.setFavorito(cursor.getInt(6));
        return contacto;
    }

    public Contacto getContacto(long id){
        Contacto contacto = null;
        SQLiteDatabase db = this.helper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Contacto.TABLE_NAME,columnToRead,DefinirTabla.Contacto._ID + " =? ",new String[]{String.valueOf(id)},null,null,null);
        if (cursor.moveToFirst()){
            contacto = leerContacto(cursor);
        }
        cursor.close();
        return contacto;
    }

    public ArrayList<Contacto> allContactos(){
        ArrayList<Contacto> contactos = new ArrayList<Contacto>();
        Cursor cursor = db.query(DefinirTabla.Contacto.TABLE_NAME,null,null,null,null,null,null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Contacto contacto = leerContacto(cursor);
            contactos.add(contacto);
            cursor.moveToNext();
        }
        cursor.close();
        return  contactos;
    }

    public void cerrar(){
        helper.close();
    }
}
